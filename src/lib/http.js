import {inject, bindable} from 'aurelia-framework';
import * as FetchClient from 'aurelia-fetch-client';

@inject( FetchClient.HttpClient )
export class Http {

    constructor( http ) {
        http.configure(config => {
            config.useStandardConfiguration();
        });

        this.http = http;
        this.json = FetchClient.json;
    }

    get(url, query, options = {}, customHeaders={}) {
        // Begin the request, but don't return yet.
        let promise = this.http.fetch(url, {
            method: 'get',
            headers: this.getHeaders( customHeaders )
        });

        return promise
            .catch(response => this.handleServerError(response));
    }

    head(url) {
        return this.http
            .fetch(url, {
                method: 'head'
            });
    }

    post(url, object) {
        return this.http
            .fetch(url, {
                method: 'post',
                body: FetchClient.json(object),
                headers: this.getHeaders()
            })
            .catch(response => this.handleServerError(response));
    }

    put(url, object) {
        return this.http
            .fetch(url, {
                method: 'put',
                body: FetchClient.json(object),
                headers: this.getHeaders()
            })
            .catch(response => this.handleServerError(response));
    }

    delete(url, object) {
        let objectData = object || {};

        return this.http
            .fetch(url, {
                method: 'delete',
                body: FetchClient.json(objectData),
                headers: this.getHeaders()
            })
            .catch(response => this.handleServerError(response));
    }

    getHeaders( customHeaders ) {
        return Object.assign({}, customHeaders);
    }

    handleServerError(response) {
        const defaultErrorMessage = "Unexpected server error";

        if (response && response.json) {
            return response
                .json()
                .then(error => {
                    let message = (error || {}).message || (error.exception || {}).message;

                    return Promise.reject(message || defaultErrorMessage);
                });
        }
        else if (response && response.message) {
            return Promise.reject(response.message);
        }

        return Promise.reject(defaultErrorMessage);
    }
}
