import {inject, bindable} from 'aurelia-framework';
import {Http} from 'lib/http';

@inject( Http )
export class Root {

    loading = false;
    personas = [];

    constructor( http ) {
        this.http = http;
    }

    attached() {
        this.getPersonas();
    }

    getPersonas() {
        let uri = 'http://fabricqa.armaturecorp.net/api/security/rest/persona/default';

        this.loading = true;

        return this.http.get( uri )
            .then( resp => resp.json() )
            .then( json => {
                this.personas = json.personas;
            })
            .finally(() => {
                this.loading = false;
            });
    }
}
